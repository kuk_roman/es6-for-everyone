import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { getFighterDetails } from './services/fightersService';

export function createFighters(fighters: IFighter[]): HTMLElement {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

async function showFighterDetails(_event: Event, fighter: IFighter): Promise<void> {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo!);
}

export async function getFighterInfo(fighterId: string): Promise<IFighterDetails> {
  const fighterInfo = await getFighterDetails(fighterId);
  return fighterInfo;
}

function createFightersSelector() {
  const selectedFighters = new Map<string, IFighterDetails>()

  return async function selectFighterForBattle(event: Event, fighter: IFighter): Promise<void> {
    const fullInfo = await getFighterInfo(fighter._id);

    if ((<HTMLInputElement>event!.target!).checked) {
      selectedFighters.set(fighter._id, fullInfo!);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      // @ts-ignore
      const winner = fight(...selectedFighters.values());
      showWinnerModal(winner);
    }
  }
}
