import { minCriticalHitPower, maxCriticalHitPower, minDodgeChance, maxDodgeChance } from "./constants/fightConstants";

export function fight(firstFighter: IFighterDetails, secondFighter: IFighterDetails): IFighterDetails {
  let firstHealth: number = firstFighter.health;
  let secondHealth: number = secondFighter.health;
  let winner: IFighterDetails | undefined = firstFighter;

  if(firstHealth <= 0)
    winner = secondFighter;

  if(secondHealth <= 0)
    winner = firstFighter;

  while(firstHealth > 0 && secondHealth > 0) {

    secondHealth -= getDamage(firstFighter, secondFighter);
    if(secondHealth <= 0)
    {
      winner = firstFighter;
      break;
    }

    firstHealth -= getDamage(secondFighter, firstFighter);
    if (firstHealth <= 0)
    {
        winner = secondFighter;2
        break;
    }
  }

  return winner;
}

export function getDamage(attacker: IFighterDetails, enemy: IFighterDetails): number {
  let damage: number= getHitPower(attacker) - getBlockPower(enemy);
  if(damage < 0)
    damage = 0;
    //console.log(`damage: ${damage}`);
  return damage;
}

export function getHitPower(fighter: IFighterDetails): number {
  const criticalHitChance = getRandomNumber(minCriticalHitPower, maxCriticalHitPower);
  const power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter: IFighterDetails): number {
  const dodgeChance = getRandomNumber(minDodgeChance, maxDodgeChance);
  const power = fighter.defense * dodgeChance;
  return power;
}

function getRandomNumber(min : number, max : number): number {
  return Math.random() * (max - min) + min;
}
