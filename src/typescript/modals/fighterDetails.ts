import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter: IFighterDetails): void {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter: IFighterDetails): HTMLElement {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: {src: source, alt: name} });
  const divElement = createElement({tagName: 'div'});
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attackElement = createElement({tagName: 'p'});
  const defenseElement = createElement({tagName: 'p'});
  const healthElement = createElement({tagName: 'p'});

  nameElement.innerText = name;
  attackElement.innerText = `Attack: ${attack}`;
  defenseElement.innerText = `Defense: ${defense}`;
  healthElement.innerText = `Health: ${health}`;
  fighterDetails.append(imgElement);
  divElement.append(nameElement);
  divElement.append(attackElement);
  divElement.append(defenseElement);
  divElement.append(healthElement);
  fighterDetails.append(divElement);

  return fighterDetails;
}
