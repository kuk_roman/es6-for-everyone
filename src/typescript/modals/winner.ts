import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showWinnerModal(fighter: IFighter): void {
  const title = 'Winner';
  const bodyElement = createWinnerInfo(fighter);
  showModal({ title, bodyElement });
}

function createWinnerInfo(fighter: IFighter): HTMLElement {
  const { name, source } = fighter;

  const winnerInfo = createElement({ tagName: 'div', className: 'modal-body' });
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: {src: source, alt: name} });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });

  nameElement.innerText = name;

  winnerInfo.append(imgElement);
  winnerInfo.append(nameElement);

  return winnerInfo;
}