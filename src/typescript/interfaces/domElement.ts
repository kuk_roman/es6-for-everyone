interface IDomElement{
    tagName: string,
    className?: string, 
    attributes?: IDictionary, 
}