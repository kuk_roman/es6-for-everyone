import { callApi } from '../helpers/apiHelper';

export async function getFighters(): Promise<IFighter[]> {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return <IFighter[]>apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: string): Promise<IFighterDetails> {
  try {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET');
    
    return <IFighterDetails>apiResult;
  } catch (error) {
    throw error;
  }
}

