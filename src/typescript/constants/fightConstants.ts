export const minCriticalHitPower = 1;
export const maxCriticalHitPower = 2;
export const minDodgeChance = 1;
export const maxDodgeChance = 2;
